#!/usr/bin/python
#prueba3.py

import time
import RPi.GPIO as GPIO

ReadPin = 18
GPIO.setmode(GPIO.BCM)
GPIO.setup(ReadPin,GPIO.IN)
#NUM_CYCLES = 50
Tiempo = 5.0
impulse_count = 0.0
endTime = time.time()+Tiempo
#for impulse_count in range(NUM_CYCLES):
while(int(time.time()) < endTime):
	GPIO.wait_for_edge(ReadPin,GPIO.FALLING) 
	impulse_count = impulse_count + 1 
	#duration = time.time()-start
	print(impulse_count)
frequency = impulse_count / Tiempo
velocidad = frequency*2.4
print("Frecuencia:"+str(frequency)+"Hz  Viento:"+str(velocidad)+"Km/Hr")

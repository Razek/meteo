#!/usr/bin/env python2.7
# demo of "BOTH" bi-directional edge detection
# script by Alex Eames http://RasPi.tv
# http://raspi.tv/?p=6791

import RPi.GPIO as GPIO
from time import sleep     # this lets us have a time delay (see line 12)

GPIO.setmode(GPIO.BCM)     # set up BCM GPIO numbering
GPIO.setup(18, GPIO.IN)    # set GPIO18 as input (button)
subidas = 0.0
bajadas = 0.0
factor = 2.4
periodo = 5.0

# Define a threaded callback function to run in another thread when events are detected
def my_callback(channel):
    if GPIO.input(18):     # if port 18 == 1
        print "Rising edge detected on 18"
	global subidas
	subidas = subidas + 1
	return(subidas)
    else:                  # if port 18 != 1
        print "Falling edge detected on 18"
	global bajadas
	bajadas = bajadas + 1
	return(bajadas)

# when a changing edge is detected on port 18, regardless of whatever 
# else is happening in the program, the function my_callback will be run
GPIO.add_event_detect(18, GPIO.BOTH, callback=my_callback)

#print "Program will finish after 30 seconds or if you press CTRL+C\n"
#print "Make sure you have a button connected, pulled down through 10k resistor"
#print "to GND and wired so that when pressed it connects"
#print "GPIO port 18 (pin 22) to GND (pin 6) through a ~1k resistor\n"

#print "Also put a 100 nF capacitor across your switch for hardware debouncing"
#print "This is necessary to see the effect we're looking for"
#raw_input("Press Enter when ready\n>")

try:
    print "When pressed, you'll see: Rising Edge detected on 18"
    print "When released, you'll see: Falling Edge detected on 18"
    sleep(periodo)         # wait 5 seconds
    print "Time's up. Finished!"
    print(subidas)
    print(bajadas)
    if(subidas <= bajadas):
    	frecuencia = float(subidas/periodo)
        velocidad = frecuencia*factor
        print("Frecuencia:"+str(frecuencia)+"Hz  Velocidad:"+str(velocidad)+"Km/hr")
    
    else:
	frecuencia = float(bajadas/periodo)
    	velocidad = frecuencia*factor	
    	print("Frecuencia:"+str(frecuencia)+"Hz  Velocidad:"+str(velocidad)+"Km/hr")


finally:                   # this block will run no matter how the try block exits
    GPIO.cleanup()         # clean up after yourself

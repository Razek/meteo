import RPi.GPIO as GPIO
import time
import Modulo_Meteo as MET
from datetime import datetime

PinLectura = 18


while True:
 
	out = open("Meteo_Data.cvs","a")
	date = str(datetime.now())
	print(date)
	frec, veloc = MET.anemometro(PinLectura)
	grad, dir = MET.veleta()
	print >> out, ";".join([date, str(frec), str(veloc), str(grad), str(dir)])
	out.close()
	time.sleep(25)

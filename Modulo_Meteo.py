#!/usr/bin/env python2.7
# Modulos para lectura de estacion metereologica
# script by Jose Luis Verdugo

import RPi.GPIO as GPIO
from time import sleep     # this lets us have a time delay 
import time, signal, sys
from Adafruit_ADS1x15 import ADS1x15


#Declaracion de Variables
#subidas = 0.0
#bajadas = 0.0
factor = 2.4
periodo = 5.0
PinLectura = 18 #GPIO utilizado en las pruebas


#Configuracion Pin Entrada
GPIO.setmode(GPIO.BCM)     # set up BCM GPIO numbering
GPIO.setup(PinLectura, GPIO.IN)    # set GPIO18 as input (button)

#Modulos para Anemometro

# Define a threaded callback function to run in another thread when events are detected
def ConteoPulsos(channel):
    
	if GPIO.input(PinLectura):     # if port PinLectura == 1
        # print "Rising edge detected on PinLectura"
		global subidas
		subidas = subidas + 1
		return(subidas)
	else:                  # if port PinLectura != 1
        # print "Falling edge detected on PinLectura"
		global bajadas
		bajadas = bajadas + 1
		return(bajadas)

# when a changing edge is detected on port PinLectura, regardless of whatever 
# else is happening in the program, the function my_callback will be run
def anemometro(PinLectura):
	#Configuracion Pin Entrada
	GPIO.setmode(GPIO.BCM)     # set up BCM GPIO numbering
	GPIO.setup(PinLectura, GPIO.IN)    # set GPIO18 as input (button)
	
	GPIO.add_event_detect(PinLectura, GPIO.BOTH, callback=ConteoPulsos)
	global subidas
	global bajadas
	subidas = 0.0
	bajadas = 0.0
	
	try:
    		#print "When pressed, you'll see: Rising Edge detected on PinLectura"
    		#print "When released, you'll see: Falling Edge detected on PinLectura"
    		sleep(periodo)         # wait 5 seconds
    		#print "Tiempo de muestreo de veleta finalizado!"
    		#print(subidas)
    		#print(bajadas)
    	
		if(subidas <= bajadas):
    			frecuencia = float(subidas/periodo)
        		velocidad = frecuencia*factor
        		print("Frecuencia:"+str(frecuencia)+"Hz  Velocidad:"+str(velocidad)+"Km/hr")
			return(frecuencia, velocidad)    

    		else:
			frecuencia = float(bajadas/periodo)
    			velocidad = frecuencia*factor	
    			print("Frecuencia:"+str(frecuencia)+"Hz  Velocidad:"+str(velocidad)+"Km/hr")
			return(frecuencia, velocidad)

	finally:                   # this block will run no matter how the try block exits
    		GPIO.cleanup()         # clean up after yourself
		subidas = 0.0
		bajadas = 0.0
			
			
# Modulos para Veleta

#def signal_handler(signal, frame):

 #   	print '\nYou pressed Ctrl+C!'
 #   	sys.exit(0)

#signal.signal(signal.SIGINT, signal_handler)






def veleta():

	# Initialize the ADC using the default mode (use default I2C address)
	# Set this to ADS1015 or ADS1115 depending on the ADC you are using!
	# adc = ADS1x15(ic=ADS1115) #descomentar en caso de utilizar el ADS1115
    	ADS1015 = 0x00
	adc = ADS1x15(ic=ADS1015)
	pga = 0256 #Ganancia del ADC
	sps = 250 #Muestras por segundo
	
	adc1 = adc.readADCSingleEnded(0, pga, sps)
	Valor = int(round(adc1))
	
	if(Valor >=104 and Valor <= 106):
		grados = "0"
		direccion = "norte"
	elif(Valor >=53 and Valor <= 55):
		grados = "22.5"
		direccion = "NNE-Nornordeste"
	elif(Valor >=61 and Valor <= 63):
		grados = "45"
		direccion = "NE-Nordeste"
	elif(Valor == 11):
		grados = "67.5"
		direccion = "ENE-Estenordeste"
	elif(Valor >= 12 and Valor <= 13):
		grados = "90"
		direccion = "E-Este"
	elif(Valor >=7 and Valor <= 9):
		grados = "112.5"
		direccion = "ESE-Estesudeste"
	elif(Valor >=23 and Valor <= 25):
		grados = "135"
		direccion = "SE-Surdeste"
	elif(Valor >=15 and Valor <= 17):
		grados = "157"
		direccion = "SSE-Sursudeste"
	elif(Valor >=37 and Valor <= 39):
		grados = "180"
		direccion = "Sur"
	elif(Valor >=31 and Valor <= 33):
		grados = "202.5"
		direccion = "SSO-Sudsudoeste"
	elif(Valor >=83 and Valor <= 85):
		grados = "225"
		direccion = "SO-Sudoeste"
	elif(Valor >=79 and Valor <= 81):
		grados = "247.5"
		direccion = "OSO-Oestesudoeste"
	elif(Valor >=125 and Valor <= 128):
		grados = "270"
		direccion = "O-Oeste"
	elif(Valor >=110 and Valor <= 112):
		grados = "292.5"
		direccion = "ONO-Oesnoroeste"
	elif(Valor >=118 and Valor <= 120):
		grados = "315"
		direccion = "NO-Noroeste"
	elif(Valor >=93 and Valor <= 95):
		grados = "337.5"
		direccion = "NNO-Nornoroeste"
	else:
		grados = "indef"
		direccion = "indef"

	print ("Grados:"+grados+" "+"Direccion:"+direccion+" "+"ADCVal:"+str(Valor));
       	return(grados, direccion)
	#time.sleep(0.25)

if __name__=='__main__':
	main()

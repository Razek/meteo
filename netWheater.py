#!/usr/bin/env python
# Modulos para lectura de estacion metereologica
# script by Jose Luis Verdugo

import RPi.GPIO as GPIO
import time, signal, sys
from Adafruit_ADS1x15 import ADS1x15
import threading
import os

## PIN_LECTURE = 18 #GPIO utilizado en las pruebas
## self.__PERIOD = 5.0

# Clase para Anemometro
class Anemometro:
	def __init__(self, pin_lecture = 5, periodo = 5.0, factor = 2.4):
		self.__PIN_LECTURE = pin_lecture #GPIO utilizado en las pruebas
		GPIO.setmode(GPIO.BCM)     # set up BCM GPIO numbering
		GPIO.setup(self.__PIN_LECTURE, GPIO.IN, pull_up_down=GPIO.PUD_UP)    # set GPIO18 as input (button)
		GPIO.add_event_detect(self.__PIN_LECTURE, GPIO.BOTH, callback=self.__conteoPulsos)
		self.__FACTOR = factor
		self.__PERIOD = periodo
		self.__subidas = 0.0
		self.__bajadas = 0.0
		self.frecuencia = 0.0
		self.velocidad = 0.0
		thread = threading.Thread(target=self.__looper, args=())
		thread.daemon = True
		thread.start()
	
	def __looper(self):
		while True:
			try:			
				time.sleep(self.__PERIOD)	
				if(self.__subidas <= self.__bajadas):
					self.frecuencia = float(self.__subidas/self.__PERIOD)
					self.velocidad = self.frecuencia*self.__FACTOR
					#print("Frecuencia:"+str(self.frecuencia)+"Hz  Velocidad:"+str(self.velocidad)+"Km/hr")
				else:
					self.frecuencia = float(self.__subidas/self.__PERIOD)
					self.velocidad = self.frecuencia*self.__FACTOR	
					#print("Frecuencia:"+str(self.frecuencia)+"Hz  Velocidad:"+str(self.velocidad)+"Km/hr" )
			except:
				print("Error Reading Anemometer")

			finally:                   # this block will run no matter how the try block exits
					self.__subidas = 0.0
					self.__bajadas = 0.0
				
	def __conteoPulsos(self,channel):
		if GPIO.input(self.__PIN_LECTURE):
#			print("UP")
			self.__subidas = self.__subidas + 1.0
		else:   
#			print("DOWN")          
			self.__bajadas = self.__bajadas + 1.0


class Veleta:
	def __init__(self, period = 1.0):
		# Initialize the ADC using the default mode (use default I2C address)
		# Set this to ADS1015 or ADS1115 depending on the ADC you are using!
		# adc = ADS1x15(ic=ADS1115) #descomentar en caso de utilizar el ADS1115
		ADS1015 = 0x00
		self.__adc = ADS1x15(ic=ADS1015)
		self.__pga = 0256 #Ganancia del ADC
		self.__sps = 250 #Muestras por segundo
		self.__adc1 = 0
		self.value = 0
		self.grados = 0
		self.direccion = 0
		self.__PERIOD = period
		#thread = threading.Thread(target=self.__looper, args=())
		#thread.daemon = True
		#thread.start()

	def getDirection(self):
		#while True:
		try:		
			self.__adc1 = self.__adc.readADCSingleEnded(0, self.__pga, self.__sps)
			self.value = int(round(self.__adc1))
		
			if(self.value >= 82 and self.value <= 84):#83
				self.grados = 0
				self.direccion = "N"
			elif(self.value >= 59 and self.value <= 61):#60
				self.grados = 22.5
				self.direccion = "NNE"
			elif(self.value >= 63 and self.value <= 65):#64
				self.grados = 45
				self.direccion = "NE"
			#elif(self.value == 36):
			#	self.grados = 67.5
			#	self.direccion = "ENE"
			elif(self.value >= 35 and self.value <= 37):#36
				self.grados = "90"
				self.direccion = "E"
			elif(self.value >= 33 and self.value <= 34):#34
				self.grados = "112.5"
				self.direccion = "ESE"
			elif(self.value >= 43 and self.value <= 45):#44
				self.grados = "135"
				self.direccion = "SE"
			elif(self.value >= 38 and self.value <= 40):#39
				self.grados = "157"
				self.direccion = "SSE"
			elif(self.value >= 51 and self.value <= 53):#52
				self.grados = "180"
				self.direccion = "S"
			elif(self.value >= 48 and self.value <= 50):#49
				self.grados = "202.5"
				self.direccion = "SSO"
			elif(self.value >= 74 and self.value <= 75):#74
				self.grados = "225"
				self.direccion = "SO"
			elif(self.value >= 72 and self.value <= 73):#73
				self.grados = "247.5"
				self.direccion = "OSO"
			elif(self.value >= 90 and self.value <= 91):#90
				self.grados = "270"
				self.direccion = "O"
			elif(self.value >= 84 and self.value <= 86):#85
				self.grados = "292.5"
				self.direccion = "ONO"
			elif(self.value >= 87 and self.value <= 89):#88
				self.grados = "315"
				self.direccion = "NO"
			elif(self.value >= 77 and self.value <= 79):#78
				self.grados = "337.5"
				self.direccion = "NNO"
			else:
				self.grados = "e"
				self.direccion = -1
			time.sleep(self.__PERIOD)

		except:
			#print("Value: {}").format(self.value)
			self.grados = "ee"
			self.direccion = -2

		#return self.grados, self.direccion, self.value

class Pluviometro:
	def __init__(self, pin_lecture = 6, periodo = 1.0, factor = 0.2794):
		self.__PIN_LECTURE = pin_lecture #GPIO utilizado en las pruebas
		GPIO.setmode(GPIO.BCM)     # set up BCM GPIO numbering
		GPIO.setup(self.__PIN_LECTURE, GPIO.IN, pull_up_down=GPIO.PUD_UP)    # set GPIO18 as input (button)
		GPIO.add_event_detect(self.__PIN_LECTURE, GPIO.BOTH, callback=self.__conteoPulsos)
		self.__FACTOR = factor
		self.__PERIOD = periodo
		self.__subidas = 0.0
		self.__bajadas = 0.0
		self.precipitacion = 0.0
		#thread = threading.Thread(target=self.__looper, args=())
		#thread.daemon = True
		#thread.start()
	
	def getRain(self):
		#while True:
		try:			
			time.sleep(self.__PERIOD)	
			if(self.__subidas <= self.__bajadas):
				self.precipitacion = self.__subidas*self.__FACTOR	
			else:
				self.precipitacion = self.__subidas*self.__FACTOR		
		except:
			print("Error Reading Wind Vane")

		#return self.precipitacion

				
	def __conteoPulsos(self,channel):
		if GPIO.input(self.__PIN_LECTURE):
			self.__subidas = self.__subidas + 1.0
		else:            
			self.__bajadas = self.__bajadas + 1.0

	def setToZero(self):
		self.__subidas = 0.0
		self.__bajadas = 0.0
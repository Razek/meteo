#!/usr/bin/env python


from netWheater import *
from DHT22 import *
import pigpio

pi = pigpio.pi()
#ane = Anemometro()
vel = Veleta()
ane = Anemometro()
plu = Pluviometro()
dht = DHT22(pi)

while True:
	dht.trigger()
	vel.getDirection()
	plu.getRain()
	print("Temperatura:{}, Humedad:{}").format( dht.temperature(), dht.humidity())
	print("Grados:{}, Direccion:{}, Value:{}").format(vel.grados, vel.direccion, vel.value)
	print("Vel:{}, Frec:{}").format(ane.velocidad, ane.frecuencia)
	print("Precipitacion:{}").format(plu.precipitacion)
	time.sleep(5)



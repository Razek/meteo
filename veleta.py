#!/usr/bin/python



import time, signal, sys

from Adafruit_ADS1x15 import ADS1x15


def signal_handler(signal, frame):

    	print '\nYou pressed Ctrl+C!'

    	sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)


ADS1015 = 0x00  # 12-bit ADC

ADS1115 = 0x01  # 16-bit ADC


# Initialize the ADC using the default mode (use default I2C address)

# Set this to ADS1015 or ADS1115 depending on the ADC you are using!

# adc = ADS1x15(ic=ADS1115)

adc = ADS1x15(ic=ADS1015)


print

print (">>>Press Ctrl+C to quit<<<");

pga = 0256
sps = 250

while True:

	#adc1 = adc.readADCSingleEnded(2)

	adc1 = adc.readADCSingleEnded(0, pga, sps)
	Valor = int(round(adc1))
	if(Valor >=104 and Valor <= 106):
		grados = "0"
		direccion = "norte"
	elif(Valor >=53 and Valor <= 55):
                grados = "22.5"
		direccion = "NNE-Nornordeste"
        elif(Valor >=61 and Valor <= 63):
		grados = "45"
		direccion = "NE-Nordeste"
	elif(Valor == 11):
		grados = "67.5"
		direccion = "ENE-Estenordeste"
	elif(Valor >= 12 and Valor <= 13):
		grados = "90"
		direccion = "E-Este"
	elif(Valor >=7 and Valor <= 9):
		grados = "112.5"
		direccion = "ESE-Estesudeste"
	elif(Valor >=23 and Valor <= 25):
		grados = "135"
		direccion = "SE-Surdeste"
	elif(Valor >=15 and Valor <= 17):
		grados = "157"
		direccion = "SSE-Sursudeste"
	elif(Valor >=37 and Valor <= 39):
		grados = "180"
		direccion = "Sur"
	elif(Valor >=31 and Valor <= 33):
		grados = "202.5"
		direccion = "SSO-Sudsudoeste"
	elif(Valor >=83 and Valor <= 85):
		grados = "225"
		direccion = "SO-Sudoeste"
	elif(Valor >=79 and Valor <= 81):
		grados = "247.5"
		direccion = "OSO-Oestesudoeste"
	elif(Valor >=126 and Valor <= 128):
		grados = "270"
		direccion = "O-Oeste"
	elif(Valor >=110 and Valor <= 112):
		grados = "292.5"
		direccion = "ONO-Oesnoroeste"
	elif(Valor >=118 and Valor <= 120):
		grados = "315"
		direccion = "NO-Noroeste"
	elif(Valor >=93 and Valor <= 95):
		grados = "337.5"
		direccion = "NNO-Nornoroeste"
	else:
		grados = "indef"
		direccion = "indef"

	print ("Grados:"+grados+" "+"Direccion:"+direccion+" "+"ADCVal:"+str(Valor));
       	time.sleep(0.25)
                
